(function (){
    "use strict";
    var $form = $('form');

    $('button').on('click', function(e) {
      if ($('button').attr('name') == 'save') {
        savePensum(e);
      }
      if ($('button').attr('name') == 'edit') {
        saveEditPensum(e);
      }
    });

    $('.syllabus-edit').on('click', editPensum);

    function savePensum(e) {
        e.preventDefault();
        $.post(window.location.pathname, $form.serialize())
                .done(function(data) {
                    if (data['status'] == 200) {
                        window.location.reload();
                    }

                })
    }

    function editPensum(e) {
      e.preventDefault();
      var syllabus = $(e.currentTarget).data('id');
      $.get('/pensum/programa/get/' + syllabus, show_syllabus)
    }

    function saveEditPensum(e) {
      e.preventDefault();

      console.log(window.location.pathname + 'edit/');

      var $id = $('#id_id').val()

      $.post(window.location.pathname + 'edit/' + $id + '/', $form.serialize())
                .done(function(data) {
                    if (data['status'] == 200) {
                        window.location.reload();
                    }

                })
    }

    function show_syllabus (data) {
      $('#id_matter').val(data.matter);
      $('#id_teacher').val(data.teacher);
      $('#id_hours').val(data.hours);
      $('#id_hours_week').val(data.hours_week);
      $('#id_date_init').val(data.date_init);
      $('#id_date_end').val(data.date_end);
      $('#id_schedule').val(data.schedule);
      // $('#id_day_week').val(data.day_week);
      $('#id_limit_report').val(data.limit_report);
      $('input[name=day_week]').prop('checked', false);
      $.each(data.day_week, function() {
        var $day_week = $('input[name=day_week][value='+this+']');
        if ($day_week.val() == this) {
          $day_week.prop('checked', true)
        }
      });
      $('<input type="hidden" id="id_id" name="id" value="' + data.id + '">').appendTo($form);
      $('<input type="hidden" id="id_syllabus" name="syllabus" value="' + data.syllabus + '">').appendTo($form);
      $('button').attr('name', 'edit')
    }


})();