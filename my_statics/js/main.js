(function() {
    'use strict';

    $('.User').on('click', toggleMenu);

    function toggleMenu(e) {
        var menu = $('.User-menu');
        menu.slideToggle().removeClass('is_hidden')
    }
})();