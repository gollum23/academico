(function() {
    "use strict";

    $('#id_birth_date').datepick({
        dateFormat: 'yyyy-mm-dd'
    });

    $('#id_home_department' ).on('change', function() {
        var id_department = this.value;
        console.log('cambiando')
        $('#id_home_town' ).find('option').each(function() {
            if($(this).data('department')!=id_department){
                $(this ).hide();
            }
            else {
                $(this ).show();
            }
        } )
    });

})();