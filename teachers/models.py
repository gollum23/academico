from django.contrib.auth.models import User
from django.db import models

from helpers import dictionaries
from home.models import TimeStampUser


class Teacher(TimeStampUser):
    user = models.OneToOneField(
        User,
        verbose_name='Usuario'
    )

    document_type = models.CharField(
        verbose_name='Tipo de documento',
        max_length=2,
        choices=dictionaries.DOCUMENT_TYPE,
        default='CC'
    )

    document_number = models.BigIntegerField(
        verbose_name='Número de documento'
    )

    address = models.CharField(
        verbose_name='Dirección',
        max_length=250,
    )

    phone = models.BigIntegerField(
        verbose_name='Teléfono'
    )

    cellphone = models.BigIntegerField(
        verbose_name='Celular'
    )

    class Meta:
        verbose_name = 'Profesor'
        verbose_name_plural = 'Profesores'
        default_permissions = ('add', 'change', 'delete', 'view',)

    def __str__(self):
        return '{} {}'.format(self.user.first_name, self.user.last_name)

    def get_full_name(self):
        return '{} {}'.format(self.user.first_name, self.user.last_name)


# class AssignTeacher(TimeStampUser):
#     teacher = models.ForeignKey(
#         Teacher,
#         verbose_name='Profesor'
#     )
#     matter = models.ForeignKey(
#         Syllabus,
#         verbose_name='Materia'
#     )
#
#     class Meta:
#         verbose_name = 'Asignación de profesores'
#         verbose_name_plural = 'Asignación de profesores'
#         default_permissions = ('add', 'change', 'delete', 'view',)
#
#     def __str__(self):
#         return '{} - {} - {}'.format(self.teacher.get_full_name(), self.matter.name_matter)
