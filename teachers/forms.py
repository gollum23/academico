from django import forms
from .models import Teacher


class TeacherForm(forms.ModelForm):

    username = forms.CharField(
        label='Nombre de usuario'
    )
    password = forms.CharField(
        widget=forms.PasswordInput(),
        label='Contraseña',
    )
    first_name = forms.CharField(
        label='Nombres'
    )
    last_name = forms.CharField(
        label='Apellidos'
    )
    email = forms.EmailField(
        label='Correo electrónico'
    )

    class Meta:
        model = Teacher
        exclude = ('user', 'created_by', 'updated_by',)
        widgets = {
            'document_number': forms.TextInput(),
            'phone': forms.TextInput(),
            'cellphone': forms.TextInput()
        }


class TeacherEditForm(forms.ModelForm):

    username = forms.CharField(
        label='Nombre de usuario'
    )
    first_name = forms.CharField(
        label='Nombres'
    )
    last_name = forms.CharField(
        label='Apellidos'
    )
    email = forms.EmailField(
        label='Correo electrónico'
    )

    class Meta:
        model = Teacher
        exclude = ('user', 'created_by', 'updated_by',)
