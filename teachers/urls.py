from django.conf.urls import url

from .views import ListTeachersView, AddTeacherView, DetailTeacherView, EditTeacherView

urlpatterns = [
    url(r'^$', ListTeachersView.as_view(), name='list'),
    url(r'^agregar/$', AddTeacherView.as_view(), name='add'),
    url(r'^detalle/(?P<pk>\d+)/$', DetailTeacherView.as_view(), name='detail'),
    url(r'^editar/(?P<pk>\d+)/$', EditTeacherView.as_view(), name='edit'),
]



