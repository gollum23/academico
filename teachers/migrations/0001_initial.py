# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('syllabus', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='AssignTeacher',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', serialize=False, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('created_by', models.ForeignKey(to=settings.AUTH_USER_MODEL, related_name='teachers_assignteacher_ownership')),
                ('matter', models.ForeignKey(verbose_name='Materia', to='syllabus.Syllabus')),
            ],
            options={
                'verbose_name_plural': 'Asignación de profesores',
                'verbose_name': 'Asignación de profesores',
                'default_permissions': ('add', 'change', 'delete', 'view'),
            },
        ),
        migrations.CreateModel(
            name='Teacher',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', serialize=False, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('document_type', models.CharField(choices=[('TI', 'Tarjeta de Identidad'), ('CC', 'Cedula de Ciudadanía'), ('CE', 'Cedula de Extranjería')], default='CC', verbose_name='Tipo de documento', max_length=2)),
                ('document_number', models.BigIntegerField(verbose_name='Número de documento')),
                ('address', models.CharField(verbose_name='Dirección', max_length=250)),
                ('phone', models.BigIntegerField(verbose_name='Teléfono')),
                ('cellphone', models.BigIntegerField(verbose_name='Celular')),
                ('created_by', models.ForeignKey(to=settings.AUTH_USER_MODEL, related_name='teachers_teacher_ownership')),
                ('updated_by', models.ForeignKey(to=settings.AUTH_USER_MODEL, related_name='teachers_teacher_updated')),
                ('user', models.OneToOneField(verbose_name='Usuario', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name_plural': 'Profesores',
                'verbose_name': 'Profesor',
                'default_permissions': ('add', 'change', 'delete', 'view'),
            },
        ),
        migrations.AddField(
            model_name='assignteacher',
            name='teacher',
            field=models.ForeignKey(verbose_name='Profesor', to='teachers.Teacher'),
        ),
        migrations.AddField(
            model_name='assignteacher',
            name='updated_by',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL, related_name='teachers_assignteacher_updated'),
        ),
    ]
