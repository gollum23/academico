# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('teachers', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='assignteacher',
            name='created_by',
        ),
        migrations.RemoveField(
            model_name='assignteacher',
            name='matter',
        ),
        migrations.RemoveField(
            model_name='assignteacher',
            name='teacher',
        ),
        migrations.RemoveField(
            model_name='assignteacher',
            name='updated_by',
        ),
        migrations.DeleteModel(
            name='AssignTeacher',
        ),
    ]
