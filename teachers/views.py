from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.contrib.auth.models import User, Group
from django.core.urlresolvers import reverse_lazy
from django.views.generic import ListView, CreateView, DetailView, UpdateView

from .models import Teacher
from .forms import TeacherForm, TeacherEditForm


class ListTeachersView(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    permission_required = ('teachers.view_teacher',)
    model = Teacher
    template_name = 'teacher_list.html'


class AddTeacherView(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    permission_required = ('teachers.add_teacher',)
    model = Teacher
    form_class = TeacherForm
    template_name = 'teacher_add_edit.html'
    success_url = reverse_lazy('teacher:list')

    def form_valid(self, form):
        """
        Form with extra data, create user and complete basic data, assign to group 'Profesor', save both objects
        """""
        username = form.cleaned_data['username']
        email = form.cleaned_data['email']
        password = form.cleaned_data['password']
        first_name = form.cleaned_data['first_name']
        last_name = form.cleaned_data['last_name']
        user = User.objects.create_user(username, email, password)
        user.first_name = first_name
        user.last_name = last_name
        user.groups.add(Group.objects.get(name='Profesor'))
        user.save()

        teacher = form.save(commit=False)
        teacher.user = user
        teacher.created_by = self.request.user
        teacher.updated_by = self.request.user
        teacher.save()
        messages.success(self.request, 'Profesor creado con éxito')
        return super(AddTeacherView, self).form_valid(form)


class DetailTeacherView(LoginRequiredMixin, PermissionRequiredMixin, DetailView):
    permission_required = ('teachers.view_teacher',)
    model = Teacher
    template_name = 'teacher_detail.html'


class EditTeacherView(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    permission_required = ('teachers.change_teacher',)
    model = Teacher
    form_class = TeacherEditForm
    template_name = 'teacher_add_edit.html'
    success_url = reverse_lazy('teacher:list')
    data = {}

    def get_context_data(self, **kwargs):
        teacher = Teacher.objects.get(pk=self.kwargs['pk'])
        user = teacher.user
        self.data = {
            'first_name': user.first_name,
            'last_name': user.last_name,
            'email': user.email,
            'username': user.username,
            'document_type': teacher.document_type,
            'document_number': teacher.document_number,
            'address': teacher.address,
            'phone': teacher.phone,
            'cellphone': teacher.cellphone
        }
        context = super(EditTeacherView, self).get_context_data(**kwargs)
        context['form'] = self.form_class(initial=self.data)
        return context

    def form_valid(self, form):
        """
        Form with extra data, create user and complete basic data, assign to group 'Profesor', save both objects
        """""
        username = form.cleaned_data['username']
        email = form.cleaned_data['email']
        first_name = form.cleaned_data['first_name']
        last_name = form.cleaned_data['last_name']
        user = User.objects.get(username=username)
        user.email = email
        user.first_name = first_name
        user.last_name = last_name
        user.save()

        teacher = form.save(commit=False)
        teacher.user = user
        teacher.updated_by = self.request.user
        teacher.save()
        messages.success(self.request, 'Profesor actualizado con éxito')
        return super(EditTeacherView, self).form_valid(form)
