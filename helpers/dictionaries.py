# Tupla tipo de documento
DOCUMENT_TYPE = (
    ('TI', 'Tarjeta de Identidad'),
    ('CC', 'Cedula de Ciudadanía'),
    ('CE', 'Cedula de Extranjería'),
)

# Tupla género
GENDER = (
    ('M', 'Masculino'),
    ('F', 'Femenino'),
    ('O', 'LGBTI'),
)

# Tupla estado civil
CIVIL_STATE = (
    ('SO', 'Soltero(a)'),
    ('CA', 'Casado(a)'),
    ('UL', 'Union libre'),
    ('SE', 'Separado(a)'),
    ('VI', 'Viudo(a)'),
)

# Tupla grupo étnico
ETHNIC = (
    ('No', 'Ninguno'),
    ('I', 'Indígena'),
    ('N', 'Negro'),
    ('A', 'Afrocolombiano'),
    ('P', 'Palenquero'),
    ('RA', 'Raizal del Archipiélago'),
    ('RO', 'Rom, Gitano'),
)

# Tupla Discapacidad
DISABILITY_TYPE = (
    ('N', 'Ninguna'),
    ('V', 'Visual'),
    ('A', 'Auditiva'),
    ('FS', 'Física miembros superiores'),
    ('FI', 'Física miembros inferiores'),
    ('I', 'Intelectual - cognitiva'),
    ('M', 'Mental - psicosocial'),
    ('T', 'Talla baja'),
)

# Tupla tipo de sangre
BLOOD_TYPE = (
    ('O+', 'O+'),
    ('O-', 'O-'),
    ('A+', 'A+'),
    ('A-', 'A-'),
    ('B+', 'B+'),
    ('B-', 'B-'),
    ('AB+', 'AB+'),
    ('AB-', 'AB-'),
)

# Tupla nivel de educación
EDUCATION_LEVEL = (
    ('primaria', 'Primaria'),
    ('secundaria', 'Secundaria'),
    ('tecnico', 'Técnico'),
    ('tecnolog', 'Tecnólogo'),
    ('universitario', 'Universitario'),
    ('profesional', 'Profesional'),
    ('ninguno', 'Ninguno'),
)

# Tupla tipo de contacto
CONTACT_TYPE = (
    ('1', 'Pariente'),
    ('2', 'Amigo'),
    ('3', 'Otro'),
)

# Tupla estado del estudiante
STUDENT_STATE = (
    ('a', 'Activo'),
    ('g', 'Graduado'),
    ('r', 'Retirado'),
    ('re', 'Reintegro'),
    ('ap', 'Aplazado'),
)

# Tupla jornada de estudio
DAY_TRIP = (
    ('M', 'Mañana'),
    ('T', 'Tarde'),
    ('U', 'Unica'),
)

# Tupla dias de la semana
DAYS_OF_WEEK = (
    ('lu', 'Lunes'),
    ('ma', 'Martes'),
    ('mi', 'Miercoles'),
    ('ju', 'Jueves'),
    ('vi', 'Viernes'),
    ('sa', 'Sabado'),
)