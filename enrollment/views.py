from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.core.urlresolvers import reverse_lazy
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from .forms import EnrollmentForm
from .models import Enrollment


class EnrollmentListView(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    model = Enrollment
    permission_required = ('enrollment.view_enrollment',)
    template_name = 'enrollment_home.html'


class EnrollmentView(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    model = Enrollment
    form_class = EnrollmentForm
    permission_required = ('enrollment.add_enrollment',)
    success_url = reverse_lazy('enrollment:home')
    template_name = 'enrollment_home.html'

    def form_valid(self, form):
        enrollment = form.save(commit=False)
        enrollment.created_by = self.request.user
        enrollment.updated_by = self.request.user
        enrollment.save()
        messages.success(self.request, 'Estudiante matriculado con exito')
        return super(EnrollmentView, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(EnrollmentView, self).get_context_data(**kwargs)
        context['enrollment_list'] = self.model.objects.all()
        return context


class EnrollmentEditView(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    model = Enrollment
    form_class = EnrollmentForm
    permission_required = ('enrollment.change_enrollment',)
    success_url = reverse_lazy('enrollment:home')
    template_name = 'enrollment_home.html'

    def form_valid(self, form):
        enrollment = form.save(commit=False)
        enrollment.created_by = self.request.user
        enrollment.updated_by = self.request.user
        enrollment.save()
        messages.success(self.request, 'Matricula editada con exito')
        return super(EnrollmentEditView, self).form_valid(form)


class EnrollmentDeleteView(LoginRequiredMixin, PermissionRequiredMixin, DeleteView):
    model = Enrollment
    permission_required = ('enrollment.delete_enrollment',)
    success_url = reverse_lazy('enrollment:home')
    template_name = 'enrollment_delete.html'
