# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('student', '0001_initial'),
        ('syllabus', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Enrollment',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', serialize=False, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('created_by', models.ForeignKey(to=settings.AUTH_USER_MODEL, related_name='enrollment_enrollment_ownership')),
                ('program', models.ForeignKey(verbose_name='Oferta academica', to='syllabus.AcademicOffer')),
                ('student', models.ForeignKey(verbose_name='Estudiante', to='student.Student')),
                ('updated_by', models.ForeignKey(to=settings.AUTH_USER_MODEL, related_name='enrollment_enrollment_updated')),
            ],
            options={
                'verbose_name_plural': 'Inscripciones',
                'verbose_name': 'Inscripción',
                'default_permissions': ('add', 'change', 'delete', 'view'),
            },
        ),
    ]
