from django import forms
from .models import Enrollment
from student.models import Student
from syllabus.models import AcademicOffer


class EnrollmentForm(forms.ModelForm):

    class Meta:
        model = Enrollment
        exclude = ('created_by', 'updated_by',)

    def __init__(self, *args, **kwargs):
        """
        Filter active program and students
        """
        super(EnrollmentForm, self).__init__(*args, **kwargs)
        try:
            queryset_academic = AcademicOffer.objects.filter(is_current=True)
        except:
            queryset_academic = None

        try:
            queryset_students = Student.objects.filter(state='a')
        except:
            queryset_students = None
        self.fields['program'].queryset = queryset_academic
        self.fields['student'].queryset = queryset_students
