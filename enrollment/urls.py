from django.conf.urls import url

from .views import EnrollmentListView, EnrollmentView, EnrollmentEditView, EnrollmentDeleteView

urlpatterns = [
    url(r'^$', EnrollmentView.as_view(), name='home'),
    url(r'^borrar/(?P<pk>\d+)/$', EnrollmentDeleteView.as_view(), name='delete')
    # url(r'^matricular/$', EnrollmentCreateView.as_view(), name='add'),
    # url(r'^matricula/(?P<pk>\d+)/$', EnrollmentEditView.as_view(), name='edit'),
]

