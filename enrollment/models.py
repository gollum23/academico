from django.db import models

from home.models import TimeStampUser
from student.models import Student
from syllabus.models import AcademicOffer


class Enrollment(TimeStampUser):
    """
    Model with enrollment of student into academic offer inherit from TimeStampUser Abstract Class
    this is inscription model.
    """
    program = models.ForeignKey(
        AcademicOffer,
        verbose_name='Oferta academica'
    )
    student = models.ForeignKey(
        Student,
        verbose_name='Estudiante'
    )

    class Meta:
        verbose_name = 'Inscripción'
        verbose_name_plural = 'Inscripciones'
        default_permissions = ('add', 'change', 'delete', 'view',)

    def __str__(self):
        return '{}'.format(self.student.get_full_name_student())
