from django import forms

from .models import Attendance


class AttendanceForm(forms.ModelForm):

    class Meta:
        model = Attendance
        fields = ('student', 'attendance', )


class AttendanceEditForm(forms.ModelForm):

    class Meta:
        model = Attendance
        fields = ('attendance', )