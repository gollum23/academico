from django.conf.urls import url

from .views import ListMatterView, AttendanceMatterView, AttendanceMatterEditView

urlpatterns = [
    url(r'^$', ListMatterView.as_view(), name='list'),
    url(r'^(?P<pk>\d+)/$', AttendanceMatterView.as_view(), name='attendance'),
    url(r'^editar/(?P<pk>\d+)/$', AttendanceMatterEditView.as_view(), name='attendance-edit')
]

