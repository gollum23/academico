from django.contrib.auth.mixins import (LoginRequiredMixin,
                                        PermissionRequiredMixin)
from django.core.urlresolvers import reverse_lazy
from django.forms import modelformset_factory, inlineformset_factory
from django.http import HttpResponseRedirect
from django.views.generic import (ListView, CreateView, FormView)
from .forms import AttendanceForm, AttendanceEditForm
from syllabus.models import Syllabus

from .models import Attendance


class ListMatterView(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    model = Syllabus
    permission_required = ('attendance.add_attendance',)
    template_name = 'attendance_list.html'

    def get_queryset(self):
        queryset = self.model.objects.filter(
            teacher__user_id=self.request.user.id)
        # queryset = self.model.objects.filter(teacher_id=self.request.user.id)
        return queryset


class AttendanceMatterView(LoginRequiredMixin, PermissionRequiredMixin,
                           CreateView):
    model = Attendance
    form_class = AttendanceForm
    permission_required = ('attendance.add_attendance',)
    template_name = 'attendance_matter.html'
    success_url = reverse_lazy('attendance:list')

    def get_context_data(self, **kwargs):
        queryset = Syllabus.objects.filter(
            teacher__user_id=self.request.user.id)
        # queryset = Syllabus.objects.filter(teacher_id=self.request.user.id)
        matter = queryset.get(pk=self.kwargs['pk'])
        attendances = self.model.objects.filter(matter=matter).__len__()
        queryset = queryset.get(
            pk=self.kwargs['pk']).syllabus.enrollment_set.all()
        attendance_formset = inlineformset_factory(
            Syllabus, self.model, self.form_class, extra=queryset.count())

        context = super(AttendanceMatterView, self).get_context_data(**kwargs)
        context['formset'] = attendance_formset(instance=matter)
        context['students'] = queryset
        context['attendances'] = attendances
        return context

    def post(self, request, *args, **kwargs):
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        queryset = Syllabus.objects.filter(
            teacher__user_id=self.request.user.id)
        # queryset = Syllabus.objects.filter(teacher_id=self.request.user.id)
        queryset = queryset.get(
            pk=self.kwargs['pk']).syllabus.enrollment_set.all()
        attendance_formset = inlineformset_factory(
            Syllabus, self.model, self.form_class, extra=queryset.count())
        formset = attendance_formset(self.request.POST)
        if formset.is_valid():
            return self.form_valid(form, formset)
        else:
            return self.form_invalid(form, formset)

    def form_valid(self, form, formset):
        matter = Syllabus.objects.get(pk=self.kwargs['pk'])
        for f in formset:
            f.save(commit=False)
            f.instance.created_by = self.request.user
            f.instance.updated_by = self.request.user
            f.instance.matter = matter
            f.save()
        return HttpResponseRedirect(self.success_url)

    def form_invalid(self, form, formset):
        return self.render_to_response(
            self.get_context_data(form=form,
                                  formset=formset))


class AttendanceMatterEditView(LoginRequiredMixin, PermissionRequiredMixin,
                               FormView):
    model = Syllabus
    form_class = AttendanceEditForm
    permission_required = ('attendance.change_attendance',)
    template_name = 'attendance_matter_edit.html'
    success_url = reverse_lazy('attendance:list')
    formset = None

    def get_context_data(self, **kwargs):
        queryset = Attendance.objects.filter(matter=self.kwargs['pk'])
        self.formset = modelformset_factory(Attendance, self.form_class,
                                            extra=0)
        context = super(
            AttendanceMatterEditView, self).get_context_data(**kwargs)
        context['attendaces'] = queryset
        context['formset'] = self.formset(queryset=queryset)
        return context

    def post(self, request, *args, **kwargs):
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        attendance_formset = modelformset_factory(Attendance, self.form_class,
                                                  extra=0)
        formset = attendance_formset(self.request.POST)
        if formset.is_valid():
            return self.form_valid(form, formset)
        else:
            return self.form_invalid(form, formset)

    def form_valid(self, form, formset):
        for f in formset:
            if f.has_changed():
                f.save(commit=False)
                f.instance.updated_by = self.request.user
                f.save()
        return HttpResponseRedirect(self.success_url)

    def form_invalid(self, form, formset):
        return self.render_to_response(
            self.get_context_data(form=form,
                                  formset=formset))
