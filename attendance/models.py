from django.db import models

from home.models import TimeStampUser
from student.models import Student
from syllabus.models import Syllabus


class Attendance(TimeStampUser):
    student = models.ForeignKey(
        Student,
        verbose_name='Estudiante'
    )
    matter = models.ForeignKey(
        Syllabus,
        null=True
    )
    attendance = models.SmallIntegerField(
        verbose_name='Asistencia'
    )

    class Meta:
        verbose_name = 'Asistencia'
        verbose_name_plural = 'Asistencia'
        default_permissions = ('add', 'change', 'delete', 'view',)

    def __str__(self):
        return '{} - {} - {}'.format(self.student.get_full_name_student(), self.matter, self.attendance)
