# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import student.models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Student',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', serialize=False, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('first_name', models.CharField(verbose_name='Nombres', max_length=100)),
                ('last_name', models.CharField(verbose_name='Apellidos', max_length=100)),
                ('document_type', models.CharField(choices=[('TI', 'Tarjeta de Identidad'), ('CC', 'Cedula de Ciudadanía'), ('CE', 'Cedula de Extranjería')], default='CC', verbose_name='Tipo de documento', max_length=2)),
                ('document_number', models.BigIntegerField(verbose_name='Número de documento')),
                ('document_expedition', models.CharField(verbose_name='Lugar de expedición', max_length=50)),
                ('birth_date', models.DateField(verbose_name='Fecha de nacimiento')),
                ('birth_department', models.CharField(blank=True, verbose_name='Departamento de nacimiento', max_length=50)),
                ('birth_town', models.CharField(blank=True, verbose_name='Ciudad/Municipio de nacimiento', max_length=50)),
                ('nationality', models.CharField(verbose_name='Nacionalidad', max_length=50)),
                ('gender', models.CharField(choices=[('M', 'Masculino'), ('F', 'Femenino'), ('O', 'LGBTI')], verbose_name='Género', max_length=1)),
                ('email', models.EmailField(verbose_name='Correo eletrónico', max_length=254)),
                ('home_address', models.CharField(verbose_name='Dirección de residencia', max_length=250)),
                ('home_neighborhood', models.CharField(verbose_name='Barrio de residencia', max_length=50)),
                ('phone', models.BigIntegerField(verbose_name='Teléfono fijo')),
                ('cellphone', models.BigIntegerField(verbose_name='Teléfono movil')),
                ('stratum', models.SmallIntegerField(verbose_name='Estrato')),
                ('civil_state', models.CharField(choices=[('SO', 'Soltero(a)'), ('CA', 'Casado(a)'), ('UL', 'Union libre'), ('SE', 'Separado(a)'), ('VI', 'Viudo(a)')], default='SO', verbose_name='Estado civil', max_length=2)),
                ('has_sisben', models.BooleanField(verbose_name='¿Tiene sisben?', default=False)),
                ('level_sisben', models.DecimalField(decimal_places=2, blank=True, verbose_name='Nivel sisben', null=True, max_digits=8)),
                ('ethnic', models.CharField(choices=[('No', 'Ninguno'), ('I', 'Indígena'), ('N', 'Negro'), ('A', 'Afrocolombiano'), ('P', 'Palenquero'), ('RA', 'Raizal del Archipiélago'), ('RO', 'Rom, Gitano')], default='No', verbose_name='Grupo etnico', max_length=2)),
                ('disability_type', models.CharField(choices=[('N', 'Ninguna'), ('V', 'Visual'), ('A', 'Auditiva'), ('FS', 'Física miembros superiores'), ('FI', 'Física miembros inferiores'), ('I', 'Intelectual - cognitiva'), ('M', 'Mental - psicosocial'), ('T', 'Talla baja')], default='N', verbose_name='Tipo de discapacidad', max_length=2)),
                ('blood_type', models.CharField(choices=[('O+', 'O+'), ('O-', 'O-'), ('A+', 'A+'), ('A-', 'A-'), ('B+', 'B+'), ('B-', 'B-'), ('AB+', 'AB+'), ('AB-', 'AB-')], default='O+', verbose_name='Tipo de sangre', max_length=2)),
                ('education_level', models.CharField(choices=[('primaria', 'Primaria'), ('secundaria', 'Secundaria'), ('tecnico', 'Técnico'), ('tecnolog', 'Tecnólogo'), ('universitario', 'Universitario'), ('profesional', 'Profesional'), ('ninguno', 'Ninguno')], default='secundaria', verbose_name='Grado de escolaridad', max_length=20)),
                ('photo', models.ImageField(verbose_name='Foto', upload_to=student.models.photo_folder)),
                ('contact_first_name', models.CharField(verbose_name='Nombres', max_length=100)),
                ('contact_last_name', models.CharField(verbose_name='Apellidos', max_length=100)),
                ('contact_address', models.CharField(verbose_name='Dirección', max_length=250)),
                ('contact_phone', models.IntegerField(verbose_name='Teléfono')),
                ('contact_type', models.CharField(choices=[('1', 'Pariente'), ('2', 'Amigo'), ('3', 'Otro')], default='1', verbose_name='Parentesco', max_length=1)),
                ('state', models.CharField(choices=[('a', 'Activo'), ('g', 'Graduado'), ('r', 'Retirado'), ('re', 'Reintegro'), ('ap', 'Aplazado')], default='a', verbose_name='Estado', max_length=2)),
                ('created_by', models.ForeignKey(to=settings.AUTH_USER_MODEL, related_name='student_student_ownership')),
                ('home_department', models.ForeignKey(verbose_name='Departamento de residencia', to='home.Department')),
                ('home_town', models.ForeignKey(verbose_name='Municipio de residencia', to='home.Town')),
                ('updated_by', models.ForeignKey(to=settings.AUTH_USER_MODEL, related_name='student_student_updated')),
            ],
            options={
                'verbose_name_plural': 'Estudiantes',
                'ordering': ['last_name', 'first_name'],
                'verbose_name': 'Estudiante',
                'default_permissions': ('add', 'change', 'delete', 'view'),
            },
        ),
    ]
