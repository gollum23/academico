# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('student', '0002_qualification'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='qualification',
            options={'verbose_name_plural': 'Calificaciones', 'default_permissions': ('add', 'change', 'delete', 'view'), 'verbose_name': 'Calificación'},
        ),
    ]
