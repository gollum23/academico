# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2015-12-09 23:52
from __future__ import unicode_literals

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('student', '0005_auto_20151205_1300'),
    ]

    operations = [
        migrations.AlterField(
            model_name='student',
            name='stratum',
            field=models.SmallIntegerField(validators=[django.core.validators.MaxValueValidator(6), django.core.validators.MinValueValidator(1)], verbose_name='Estrato'),
        ),
    ]
