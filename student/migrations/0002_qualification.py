# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('syllabus', '0003_auto_20151107_1747'),
        ('student', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Qualification',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('qualification', models.DecimalField(verbose_name='Calificación', decimal_places=1, max_digits=2)),
                ('created_by', models.ForeignKey(related_name='student_qualification_ownership', to=settings.AUTH_USER_MODEL)),
                ('matter', models.ForeignKey(to='syllabus.Syllabus')),
                ('student', models.ForeignKey(verbose_name='Estudiante', to='student.Student')),
                ('updated_by', models.ForeignKey(related_name='student_qualification_updated', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
