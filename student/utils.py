import datetime
import os

from django.http import HttpResponse
from reportlab.lib.units import cm, inch
from reportlab.platypus import (Paragraph, Spacer, SimpleDocTemplate, Table,
                                TableStyle)
from reportlab.pdfbase.pdfmetrics import registerFont
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.lib.pagesizes import landscape, letter
from reportlab.lib import colors
from reportlab.lib.styles import ParagraphStyle
from reportlab.lib.enums import TA_CENTER, TA_JUSTIFY

PROJECT_PATH = os.path.dirname(os.path.dirname(__file__))

calibri = os.path.join(PROJECT_PATH, 'my_statics/fonts/calibri.ttf')
calibrib = os.path.join(PROJECT_PATH, 'my_statics/fonts/calibrib.ttf')

registerFont(TTFont('Calibri', calibri))
registerFont(TTFont('Calibri-Bold', calibrib))

dates_lower = ("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio",
               "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre",
               "Diciembre")
years = [
    'Dos mil Dieciseís',
    'Dos mil Diecisiete',
    'Dos mil Dieciocho',
    'Dos mil Diecinueve',
    'Dos mil Veinte',
    'Dos mil Veintiuno',
    'Dos mil Veintidos',
    'Dos mil Veintitres',
    'Dos mil Veinticuatro',
    'Dos mil Veinticinco',
    'Dos mil Veintiseís',
    'Dos mil Veintisiete',
    'Dos mil Veintiocho',
    'Dos mil Veintinueve',
    'Dos mil Treinta',
]


def certification_intensity(student):
    style_title = ParagraphStyle('defaults')
    style_title.alignment = TA_CENTER
    style_title.fontSize = 11
    style_title.leading = 12

    style_subtitle = ParagraphStyle('defaults')
    style_subtitle.alignment = TA_CENTER
    style_subtitle.fontSize = 8

    style_normal = ParagraphStyle('defaults')
    style_normal.alignment = TA_JUSTIFY
    style_normal.fontSize = 11
    style_normal.leading = 12

    style_footer = ParagraphStyle('defaults')
    style_footer.alignment = TA_JUSTIFY
    style_footer.fontSize = 6

    response = HttpResponse(content_type="application/pdf")
    filename = 'certificacion_{}.pdf'.format(student.get_full_name_student())
    response['Content-Disposition'] = 'attachment; filename={}'.format(
        filename)

    story = []

    student_name = '{} {}'.format(student.first_name.upper(),
                                  student.last_name.upper())
    identification = '{:,}'.format(student.document_number)
    identification_from = student.document_expedition
    program = student.enrollment_set.all()[0].program.program.name_program
    week_hours = str(student.enrollment_set.all()[0].program.week_hours)
    today = datetime.date.today()

    title = """<b>EL SUSCRITO COORDINADOR DE COMPETENCIAS TÉCNICAS, DE LA
    FUNDACIÓN ESCUELA TALLER DE BOGOTÁ</b>"""

    subtitle = """RESOLUCIÓN DE APROBACIÓN No 170004 DEL 05 DE MAYO
    DE 2016 EXPEDIDO POR LA SECRETARÍA DE EDUCACIÓN DE BOGOTÁ"""

    body = """Que el estudiante, <b>{}</b>, identificado con documento de
     identidad No <b>{}</b> de {}, está cursando el programa Técnico Laboral
     por Competencias en {}, con una intensidad horaria de {} horas semanales
     en el horario de 7:00 am a 5:00 pm.""".format(
        student_name, identification, identification_from, program, week_hours)

    expedition_date = """Dado en Bogotá a los {} días del mes de {} del año {}
    ({}).""".format(today.day, dates_lower[today.month-1],
                    years[today.year-2016], today.year)

    story.append(Paragraph(title, style_title))
    story.append(Paragraph(subtitle, style_subtitle))
    story.append(Spacer(6.5*inch, 1*inch))
    story.append(Paragraph('<b>CERTIFICA</b>', style_title))
    story.append(Spacer(6.5*inch, 1*inch))
    story.append(Paragraph(body, style_normal))
    story.append(Spacer(6.5*inch, 1*inch))
    story.append(Paragraph(expedition_date, style_normal))
    story.append(Spacer(6.5*inch, 2*inch))
    story.append(Paragraph('<b>ADRIANA CRUZ RAMÍREZ</b>', style_normal))
    story.append(Paragraph('Coordinadora Académica',
                           style_normal))
    story.append(Spacer(6.5 * inch, 0.7 * inch))
    story.append(Paragraph('Valido con sello seco', style_footer))

    doc = SimpleDocTemplate(response, topMargin=3.7*cm, leftMargin=3*cm)
    doc.build(story)
    return response


def certification_qualification(student):
    style_title = ParagraphStyle('defaults')
    style_title.alignment = TA_CENTER
    style_title.fontSize = 11
    style_title.leading = 12

    style_subtitle = ParagraphStyle('defaults')
    style_subtitle.alignment = TA_CENTER
    style_subtitle.fontSize = 8

    style_normal = ParagraphStyle('defaults')
    style_normal.alignment = TA_JUSTIFY
    style_normal.fontSize = 11
    style_normal.leading = 12

    style_footer = ParagraphStyle('defaults')
    style_footer.alignment = TA_JUSTIFY
    style_footer.fontSize = 6

    response = HttpResponse(content_type="application/pdf")
    filename = 'certificacion_notas_{}.pdf'.format(
        student.get_full_name_student())
    response['Content-Disposition'] = 'attachment; filename={}'.format(
        filename
    )

    story = []

    student_name = '<b>{} {}</b>'.format(student.first_name.upper(),
                                  student.last_name.upper())
    identification = '<b>{:,}</b>'.format(student.document_number)
    identification_from = student.document_expedition
    program = student.enrollment_set.all()[0].program.program.name_program
    today = datetime.date.today()
    gender_message = 'la señorita'
    if student.gender == 'M':
        gender_message = 'el señor'
    identification_msg = 'identificada'
    if student.gender == 'M':
        identification_msg = 'identificado'

    title = """<b>EL SUSCRITO COORDINADOR DE COMPETENCIAS TÉCNICAS, DE LA
        FUNDACIÓN ESCUELA TALLER DE BOGOTÁ</b>"""

    subtitle = """RESOLUCIÓN DE APROBACIÓN No 170004 DEL 05 DE MAYO
        DE 2016 EXPEDIDO POR LA SECRETARÍA DE EDUCACIÓN DE BOGOTÁ"""

    body = """Que {} {}, {} con cédula de ciudadanía No {} de {}, cursó
    y aprobó el Programa Técnico Laboral por Compentencias en {}.""".format(
        gender_message, student_name, identification_msg, identification,
        identification_from, program
    )

    expedition_date = """Expedido en Bogotá a los {} días del mes de {} del año
    {} ({}).""".format(today.day, dates_lower[today.month - 1],
                       years[today.year - 2016], today.year)

    story.append(Paragraph(title, style_title))
    story.append(Paragraph(subtitle, style_subtitle))
    story.append(Spacer(6.5 * inch, 0.3 * inch))
    story.append(Paragraph('<b>CERTIFICA</b>', style_title))
    story.append(Paragraph(body, style_normal))
    story.append(Spacer(6.5 * inch, 0.3 * inch))
    table, total_attendance, total_required = data_to_table(student)
    t = Table(table)
    t.setStyle(TableStyle([
        ('BACKGROUND', (0, 0), (4, 0), colors.lightgrey),
        ('BOX', (0, 0), (-1, -1), 0.25, colors.black),
        ('GRID', (0, 0), (-1, -1), 0.25, colors.black),
    ]))
    story.append(t)
    story.append(Spacer(6.5 * inch, 0.1 * inch))
    story.append(Paragraph('Total de horas asistidas: {}'.format(
        total_attendance), style_normal))
    story.append(Paragraph('Total de horas requeridas: {}'.format(
        total_required), style_normal))
    story.append(Spacer(6.5 * inch, 0.3 * inch))
    story.append(Paragraph(expedition_date, style_normal))
    story.append(Spacer(6.5 * inch, 2 * inch))
    story.append(Paragraph('<b>ADRIANA CRUZ RAMÍREZ</b>', style_normal))
    story.append(Paragraph('Coordinadora Académica',
                           style_normal))
    story.append(Spacer(6.5 * inch, 0.1 * inch))
    story.append(Paragraph('Valido con sello seco', style_footer))

    doc = SimpleDocTemplate(response, topMargin=3.7 * cm, leftMargin=3 * cm)
    doc.build(story)
    return response


def data_to_table(student):
    data = [['TALLER', 'HORAS DICTADAS', 'HORAS ASISTIDAS', 'NOTA']]

    matters = [m for m in student.ratenotes_set.all()]
    assitance = [a for a in student.attendance_set.all()]

    total_assitance = 0
    total_hours = 0

    for m in matters:
        for a in assitance:
            if a.matter.id == m.matter.id:
                data.append([a.matter.matter.name_matter, a.matter.hours,
                             a.attendance, m.qualification])
                total_assitance += a.attendance
                total_hours += a.matter.hours
            else:
                data.append([m.matter.matter.name_matter, m.matter.hours, 0,
                             m.qualification])
                total_hours += m.matter.hours

    return data, total_assitance, total_hours


def diploma(student):
    style_bold = ParagraphStyle('defaults')
    style_bold.alignment = TA_CENTER
    style_bold.fontSize = 14
    style_bold.leading = 16
    style_bold.fontName = 'Calibri-Bold'

    style_bold_big = ParagraphStyle('defaults')
    style_bold_big.alignment = TA_CENTER
    style_bold_big.fontSize = 22
    style_bold_big.leading = 24
    style_bold_big.fontName = 'Calibri-Bold'

    style_normal = ParagraphStyle('defaults')
    style_normal.alignment = TA_CENTER
    style_normal.fontSize = 12
    style_normal.leading = 14
    style_normal.fontName = 'Calibri'

    response = HttpResponse(content_type="application/pdf")
    filename = 'diploma_{}.pdf'.format(
        student.get_full_name_student())
    response['Content-Disposition'] = 'attachment; filename={}'.format(
        filename
    )

    story = []

    today = datetime.date.today()
    table, total_attendance, total_required = data_to_table(student)

    student_name = '{} {}'.format(student.first_name, student.last_name)
    document = '{} No. <b>{:,}</b>'.format(student.get_document_type_display(),
                                  student.document_number)
    program = student.enrollment_set.all(
        )[0].program.program.name_program.upper()
    intensity = 'Con una intensidad de {} horas asistidas.'.format(
        total_attendance)

    expedition_date = """Expedido en Bogotá, D.C a los {} días del mes de {}
            del año {} ({}).""".format(today.day, dates_lower[today.month - 1],
                                      years[today.year - 2016], today.year)

    story.append(Paragraph('LA REPÚBLICA DE COLOMBIA', style_bold))
    story.append(Spacer(8 * inch, 0.3 * inch))
    story.append(Paragraph('El ministerio de Educación Nacional, y en su '
                           'nombre la', style_normal))
    story.append(Paragraph('FUNDACIÓN ESCUELA TALLER DE BOGOTA', style_bold))
    story.append(Paragraph('Personería Jurídica 5412 de diciembre 28 de 2006 '
                           '- Resolución de Aprobación No. 170004 del 05 de '
                           'mayo de 2016', style_normal))
    story.append(Paragraph('Educación para el Trabajo y Desarrollo Humano,',
                           style_normal))
    story.append(Paragraph('Confiere a:', style_normal))

    story.append(Spacer(8 * inch, 0.3 * inch))

    story.append(Paragraph(student_name, style_bold_big))
    story.append(Paragraph(document, style_normal))

    story.append(Spacer(8 * inch, 0.3 * inch))

    story.append(Paragraph(program, style_bold_big))
    story.append(Paragraph(intensity, style_normal))
    story.append(Spacer(8 * inch, 0.3 * inch))

    story.append(Paragraph('Por haber cursado y aprobado satisfactoriamente '
                           'los estudios correspondientes a los programas '
                           'vigentes.', style_normal))

    story.append(Paragraph(expedition_date, style_normal))

    doc = SimpleDocTemplate(response, topMargin=5.5 * cm, leftMargin=1 * inch,
                            rightMargin=1 * inch, pagesize=landscape(letter))
    doc.build(story, onFirstPage=draw_template)
    return response


def draw_template(canvas, doc):
    style_normal = ParagraphStyle('defaults')
    style_normal.alignment = TA_CENTER
    style_normal.fontSize = 12
    style_normal.leading = 14
    style_normal.fontName = 'Calibri'

    style_bold = ParagraphStyle('defaults')
    style_bold.alignment = TA_CENTER
    style_bold.fontSize = 12
    style_bold.leading = 14
    style_bold.fontName = 'Calibri-Bold'

    diploma = os.path.join(PROJECT_PATH, 'my_statics/images/diploma.jpg')
    canvas.saveState()
    canvas.drawImage(diploma, 0 * inch, 0 * inch, 11 * inch, 8.5 * inch)
    sign_left = Paragraph('DIANA CATALINA PRADA ALVIS', style_bold)
    sign_right = Paragraph('ADRIANA CRUZ RAMÍREZ', style_bold)
    sign_left.wrap(3 * inch, 1 * inch)
    sign_right.wrap(3 * inch, 1 * inch)
    sign_left.drawOn(canvas, 1*inch, 1.4*inch)
    sign_right.drawOn(canvas,  7*inch, 1.4 * inch)
    text_left = Paragraph('Coordinadora General', style_normal)
    text_right = Paragraph('Coordinadora Académica',
                           style_normal)
    text_left.wrap(3 * inch, 1 * inch)
    text_right.wrap(3 * inch, 1 * inch)
    text_left.drawOn(canvas, 1 * inch, 1.2 * inch)
    text_right.drawOn(canvas, 7 * inch, 1.2 * inch)

    canvas.restoreState()
