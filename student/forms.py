from django import forms
from django.utils.encoding import force_text
from django.utils.html import escape, conditional_escape

from home.models import Town
from syllabus.models import AcademicOffer
from .models import Student


class SelectWithData(forms.Select):
    def render_option(self, selected_choices, option_value, option_label):
        obj_data = {
            obj.cod_town: {
                data_attr: getattr(obj, data_attr) for data_attr in self.data_attrs
            } for obj in self.queryset
        }

        data_text = ''
        for data_attr in self.data_attrs:
            data_text += ' data-{}="{}" '.format(
                data_attr,
                escape(force_text('')) if option_value == '' else escape(force_text(obj_data[option_value][data_attr].cod))
            )

        option_value = force_text(option_value)
        selected_html = (option_value in selected_choices) and ' selected="selected"' or ''
        return '<option value="{}"{}{}>{}</option>'.format(
            escape(option_value),
            data_text,
            selected_html,
            conditional_escape(force_text(option_label))
        )


class ModelChoiceFieldWithData(forms.ModelChoiceField):
    widget = SelectWithData

    def __init__(self, queryset, **kwargs):
        data_attrs = kwargs.pop('data_attrs')
        super(ModelChoiceFieldWithData, self).__init__(queryset, **kwargs)
        self.widget.queryset = queryset
        self.widget.data_attrs = data_attrs


class StudentForm(forms.ModelForm):
    home_town = ModelChoiceFieldWithData(
        queryset=Town.objects.all(),
        data_attrs=('department',),
        label='Municipio de residencia'
    )
    program = forms.ChoiceField(
        label='Programa al que se matricula',
    )

    class Meta:
        model = Student
        exclude = ('created_by', 'updated_by',)
        widgets = {
            'has_sisben': forms.Select(choices=(('True', 'Sí'), ('False', 'No'),)),
            'document_number': forms.TextInput(),
            'phone': forms.TextInput(),
            'cellphone': forms.TextInput(),
            'contact_phone': forms.TextInput(),
            'level_sisben': forms.TextInput(),
            'stratum': forms.TextInput()
        }

    def __init__(self, *args, **kwargs):
        super(StudentForm, self).__init__(*args, **kwargs)
        academicOffer = AcademicOffer.objects.filter(is_current=True).values_list(
            'id', 'program__name_program', 'day_trip', 'year'
        )
        academic_offer = []
        for a in academicOffer:
            academic_offer.append((str(a[0]), a[1]+' - '+a[2]+' - '+str(a[3])))
        self.fields['program'].choices = list(academic_offer)


