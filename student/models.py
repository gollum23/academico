from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models

from helpers import dictionaries
from home.models import TimeStampUser, Department, Town
from syllabus.models import Syllabus


def photo_folder(self, file):
    ext = file.split('.')[-1]
    filename = '{}_{}.{}'.format(self.first_name, self.last_name, ext)
    return '{}/{}'.format('photos', filename)


class Student(TimeStampUser):
    first_name = models.CharField(
        verbose_name='Nombres',
        max_length=100
    )
    last_name = models.CharField(
        verbose_name='Apellidos',
        max_length=100
    )
    document_type = models.CharField(
        verbose_name='Tipo de documento',
        max_length=2,
        choices=dictionaries.DOCUMENT_TYPE,
        default='CC'
    )
    document_number = models.BigIntegerField(
        verbose_name='Número de documento'
    )
    document_expedition = models.CharField(
        verbose_name='Lugar de expedición',
        max_length=50
    )
    birth_date = models.DateField(
        verbose_name='Fecha de nacimiento'
    )
    birth_department = models.CharField(
        verbose_name='Departamento de nacimiento',
        max_length=50,
        blank=True
    )
    birth_town = models.CharField(
        verbose_name='Ciudad/Municipio de nacimiento',
        max_length=50,
        blank=True
    )
    nationality = models.CharField(
        verbose_name='Nacionalidad',
        max_length=50
    )
    gender = models.CharField(
        verbose_name='Género',
        max_length=1,
        choices=dictionaries.GENDER
    )
    email = models.EmailField(
        verbose_name='Correo eletrónico'
    )
    home_address = models.CharField(
        verbose_name='Dirección de residencia',
        max_length=250
    )
    home_neighborhood = models.CharField(
        verbose_name='Barrio de residencia',
        max_length=50
    )
    home_department = models.ForeignKey(
        Department,
        verbose_name='Departamento de residencia'
    )
    home_town = models.ForeignKey(
        Town,
        verbose_name='Municipio de residencia'
    )
    phone = models.BigIntegerField(
        verbose_name='Teléfono fijo'
    )
    cellphone = models.BigIntegerField(
        verbose_name='Teléfono movil'
    )

    stratum = models.SmallIntegerField(
        verbose_name='Estrato',
        validators=[
            MaxValueValidator(6),
            MinValueValidator(1)
        ]
    )
    civil_state = models.CharField(
        verbose_name='Estado civil',
        max_length=2,
        choices=dictionaries.CIVIL_STATE,
        default='SO'
    )
    has_sisben = models.BooleanField(
        verbose_name='¿Tiene sisben?',
        default=False
    )
    level_sisben = models.DecimalField(
        verbose_name='Nivel sisben',
        decimal_places=2,
        max_digits=8,
        blank=True,
        null=True
    )
    ethnic = models.CharField(
        verbose_name='Grupo etnico',
        max_length=2,
        choices=dictionaries.ETHNIC,
        default='No'
    )
    disability_type = models.CharField(
        verbose_name='Tipo de discapacidad',
        max_length=2,
        choices=dictionaries.DISABILITY_TYPE,
        default='N'
    )
    blood_type = models.CharField(
        verbose_name='Tipo de sangre',
        max_length=2,
        choices=dictionaries.BLOOD_TYPE,
        default='O+'
    )
    education_level = models.CharField(
        verbose_name='Grado de escolaridad',
        max_length=20,
        choices=dictionaries.EDUCATION_LEVEL,
        default='secundaria'
    )
    photo = models.ImageField(
        verbose_name='Foto',
        upload_to=photo_folder
    )
    contact_first_name = models.CharField(
        verbose_name='Nombres',
        max_length=100
    )
    contact_last_name = models.CharField(
        verbose_name='Apellidos',
        max_length=100,
    )
    contact_address = models.CharField(
        verbose_name='Dirección',
        max_length=250
    )
    contact_phone = models.BigIntegerField(
        verbose_name='Teléfono'
    )
    contact_type = models.CharField(
        verbose_name='Parentesco',
        max_length=1,
        choices=dictionaries.CONTACT_TYPE,
        default='1'
    )
    state = models.CharField(
        verbose_name='Estado',
        max_length=2,
        choices=dictionaries.STUDENT_STATE,
        default='a'
    )

    class Meta:
        verbose_name = 'Estudiante'
        verbose_name_plural = 'Estudiantes'
        ordering = ['last_name', 'first_name']
        default_permissions = ('add', 'change', 'delete', 'view',)

    def __str__(self):
        return '{} {}'.format(self.last_name, self.first_name)

    def get_full_name_student(self):
        return '{} {}'.format(self.last_name, self.first_name)


# class Qualification(TimeStampUser):
#     student = models.ForeignKey(
#         Student,
#         verbose_name='Estudiante'
#     )
#     matter = models.ForeignKey(
#         Syllabus
#     )
#     qualification = models.DecimalField(
#         verbose_name='Calificación',
#         decimal_places=1,
#         max_digits=2
#     )
#
#     class Meta:
#         verbose_name = 'Calificación'
#         verbose_name_plural = 'Calificaciones'
#         default_permissions = ('add', 'change', 'delete', 'view',)
#
#     def __str__(self):
#         return '{} - {} - {}'.format(self.student.get_full_name_student(), self.matter, self.qualification)
#
#
# class Assistance(TimeStampUser):
#     student = models.ForeignKey(
#         Student,
#         verbose_name='Estudiante'
#     )
#     matter = models.ForeignKey(
#         Syllabus
#     )
#     assistance = models.SmallIntegerField(
#         verbose_name='Asistencia'
#     )
#
#     def __str__(self):
#         return '{} - {} - {}'.format(self.student.get_full_name_student(), self.matter, self.assistance)
