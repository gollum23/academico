from django.contrib import messages
from django.contrib.auth.mixins import (LoginRequiredMixin,
                                        PermissionRequiredMixin)
from django.core.urlresolvers import reverse_lazy
from django.http import HttpResponseRedirect
from django.views.generic import (ListView, CreateView, DetailView, UpdateView,
                                  View)

from syllabus.models import AcademicOffer
from .models import Student
from .forms import StudentForm
from .utils import (certification_intensity, certification_qualification,
                    diploma)
from enrollment.models import Enrollment


class ListStudentsView(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    model = Student
    template_name = 'student_list.html'
    permission_required = ('student.view_student',)

    def get_queryset(self):
        queryset = self.model.objects.exclude(state='r')
        return queryset


class AddStudentView(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    permission_required = ('student.add_student',)
    model = Student
    form_class = StudentForm
    template_name = 'student_add_edit.html'
    success_url = reverse_lazy('student:list')

    def form_valid(self, form):
        student = form.save(commit=False)
        student.created_by = self.request.user
        student.updated_by = self.request.user
        student.save()
        program = form.cleaned_data['program']
        a = AcademicOffer.objects.get(pk=program)
        p = Enrollment(program=a, student=student)
        p.created_by = self.request.user
        p.updated_by = self.request.user
        p.save()
        messages.success(self.request, 'Estudiante creado con éxito')
        return super(AddStudentView, self).form_valid(form)


class DetailStudentView(LoginRequiredMixin, PermissionRequiredMixin,
                        DetailView):
    permission_required = ('student.view_student',)
    model = Student
    template_name = 'student_detail.html'


class EditStudentView(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    permission_required = ('student.change_student',)
    model = Student
    form_class = StudentForm
    template_name = 'student_add_edit.html'
    success_url = reverse_lazy('student:list')

    def form_valid(self, form):
        student = form.save(commit=False)
        student.updated_by = self.request.user
        student.save()
        program = form.cleaned_data['program']
        a = AcademicOffer.objects.get(pk=program)
        try:
            p = Enrollment.objects.get(student=student)
            p.program = a
        except:
            p = Enrollment(program=a, student=student)
            p.created_by = self.request.user
        p.updated_by = self.request.user
        p.save()
        messages.success(self.request, 'Estudiante actualizado con éxito')
        return super(EditStudentView, self).form_valid(form)


class PrintCertificationIntensity(LoginRequiredMixin, View):

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            student = Student.objects.get(pk=self.kwargs['pk'])
            return certification_intensity(student)
        return HttpResponseRedirect('/')


class PrintCertificationQualificationView(LoginRequiredMixin, View):

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            student = Student.objects.get(pk=self.kwargs['pk'])
            return certification_qualification(student)
        return HttpResponseRedirect('/')


class PrintDiplomaView(LoginRequiredMixin, View):

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            student = Student.objects.get(pk=self.kwargs['pk'])
            return diploma(student)
        return HttpResponseRedirect('/')
