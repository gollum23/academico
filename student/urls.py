from django.conf.urls import url

from .views import (ListStudentsView, AddStudentView, DetailStudentView,
                    EditStudentView, PrintCertificationIntensity,
                    PrintCertificationQualificationView, PrintDiplomaView)

urlpatterns = [
    url(r'^$', ListStudentsView.as_view(), name='list'),
    url(r'^agregar/$', AddStudentView.as_view(), name='add'),
    url(r'^detalle/(?P<pk>\d+)/$', DetailStudentView.as_view(), name='detail'),
    url(r'^editar/(?P<pk>\d+)/$', EditStudentView.as_view(), name='edit'),
    url(r'^print_intensity/(?P<pk>\d+)/$',
        PrintCertificationIntensity.as_view(), name='print_intensity'),
    url(r'^print_qualification/(?P<pk>\d+)/$',
        PrintCertificationQualificationView.as_view(),
        name='print_qualification'),
    url(r'^print_diploma/(?P<pk>\d+)/$',
        PrintDiplomaView.as_view(),
        name='print_diploma'),
]
