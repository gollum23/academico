# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('syllabus', '0002_auto_20151021_2209'),
    ]

    operations = [
        migrations.AlterField(
            model_name='syllabus',
            name='date_end',
            field=models.DateField(verbose_name='Termina'),
        ),
        migrations.AlterField(
            model_name='syllabus',
            name='date_init',
            field=models.DateField(verbose_name='Inicia'),
        ),
        migrations.AlterField(
            model_name='syllabus',
            name='day_week',
            field=models.CharField(verbose_name='Día', max_length=2, choices=[('lu', 'Lunes'), ('ma', 'Martes'), ('mi', 'Miercoles'), ('ju', 'Jueves'), ('vi', 'Viernes'), ('sa', 'Sabado')]),
        ),
        migrations.AlterField(
            model_name='syllabus',
            name='hours',
            field=models.SmallIntegerField(verbose_name='# de horas'),
        ),
        migrations.AlterField(
            model_name='syllabus',
            name='schedule',
            field=models.CharField(verbose_name='Hora', max_length=5),
        ),
    ]
