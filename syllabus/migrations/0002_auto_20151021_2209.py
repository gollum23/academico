# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('teachers', '0002_auto_20151021_2209'),
        ('syllabus', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='syllabus',
            name='teacher',
            field=models.ForeignKey(default=0, verbose_name='Profesor', to='teachers.Teacher'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='academicoffer',
            name='day_trip',
            field=models.CharField(choices=[('M', 'Mañana'), ('T', 'Tarde'), ('U', 'Unica')], verbose_name='Jornada', max_length=1),
        ),
    ]
