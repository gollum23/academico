# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='AcademicOffer',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', serialize=False, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('year', models.SmallIntegerField(verbose_name='Año')),
                ('day_trip', models.CharField(choices=[('M', 'Mañana'), ('T', 'Tarde'), ('U', 'Unica')], verbose_name='Jornada', max_length='1')),
                ('is_current', models.BooleanField(verbose_name='Programa activo', default=True)),
                ('created_by', models.ForeignKey(to=settings.AUTH_USER_MODEL, related_name='syllabus_academicoffer_ownership')),
            ],
            options={
                'verbose_name_plural': 'Oferta acádemica',
                'verbose_name': 'Oferta acádemica',
                'default_permissions': ('add', 'change', 'delete', 'view'),
            },
        ),
        migrations.CreateModel(
            name='Matter',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', serialize=False, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('name_matter', models.CharField(verbose_name='Nombre de la materia', max_length=50)),
                ('cod_matter', models.CharField(verbose_name='Código de la materia', max_length=10)),
                ('created_by', models.ForeignKey(to=settings.AUTH_USER_MODEL, related_name='syllabus_matter_ownership')),
                ('updated_by', models.ForeignKey(to=settings.AUTH_USER_MODEL, related_name='syllabus_matter_updated')),
            ],
            options={
                'verbose_name_plural': 'Materias',
                'verbose_name': 'Materia',
                'default_permissions': ('add', 'change', 'delete', 'view'),
            },
        ),
        migrations.CreateModel(
            name='Program',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', serialize=False, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('name_program', models.CharField(verbose_name='Nombre del programa', max_length=80)),
                ('created_by', models.ForeignKey(to=settings.AUTH_USER_MODEL, related_name='syllabus_program_ownership')),
                ('updated_by', models.ForeignKey(to=settings.AUTH_USER_MODEL, related_name='syllabus_program_updated')),
            ],
            options={
                'verbose_name_plural': 'Programas',
                'verbose_name': 'Programa',
                'default_permissions': ('add', 'change', 'delete', 'view'),
            },
        ),
        migrations.CreateModel(
            name='Syllabus',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', serialize=False, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('hours', models.SmallIntegerField(verbose_name='Número de horas')),
                ('date_init', models.DateField(verbose_name='Fecha de inicio')),
                ('date_end', models.DateField(verbose_name='Fecha de terminación')),
                ('day_week', models.CharField(choices=[('lu', 'Lunes'), ('ma', 'Martes'), ('mi', 'Miercoles'), ('ju', 'Jueves'), ('vi', 'Viernes'), ('sa', 'Sabado')], verbose_name='Día de la semana', max_length=2)),
                ('schedule', models.CharField(verbose_name='Horario', max_length=5)),
                ('created_by', models.ForeignKey(to=settings.AUTH_USER_MODEL, related_name='syllabus_syllabus_ownership')),
                ('matter', models.ForeignKey(verbose_name='Materia', to='syllabus.Matter')),
                ('syllabus', models.ForeignKey(verbose_name='Programa', to='syllabus.AcademicOffer')),
                ('updated_by', models.ForeignKey(to=settings.AUTH_USER_MODEL, related_name='syllabus_syllabus_updated')),
            ],
            options={
                'verbose_name_plural': 'Pensum',
                'verbose_name': 'Pensum',
                'default_permissions': ('add', 'change', 'delete', 'view'),
            },
        ),
        migrations.AddField(
            model_name='academicoffer',
            name='program',
            field=models.ForeignKey(verbose_name='Programa', to='syllabus.Program'),
        ),
        migrations.AddField(
            model_name='academicoffer',
            name='updated_by',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL, related_name='syllabus_academicoffer_updated'),
        ),
    ]
