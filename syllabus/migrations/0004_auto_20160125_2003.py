# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2016-01-26 01:03
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('syllabus', '0003_auto_20151107_1747'),
    ]

    operations = [
        migrations.AlterField(
            model_name='syllabus',
            name='hours',
            field=models.SmallIntegerField(verbose_name='# de horas resolución'),
        ),
    ]
