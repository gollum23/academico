from django import forms

from .models import Program, Matter, AcademicOffer, Syllabus


class ProgramForm(forms.ModelForm):

    class Meta:
        model = Program
        exclude = ('created_by', 'updated_by',)


class MatterForm(forms.ModelForm):

    class Meta:
        model = Matter
        exclude = ('created_by', 'updated_by',)


class AcademicOfferForm(forms.ModelForm):

    class Meta:
        model = AcademicOffer
        exclude = ('created_by', 'updated_by',)
        widgets = {
            'year': forms.TextInput()
        }


class SyllabusForm(forms.ModelForm):

    class Meta:
        model = Syllabus
        exclude = ('created_by', 'updated_by', 'syllabus')
