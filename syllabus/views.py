from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.core.urlresolvers import reverse_lazy
from django.http import JsonResponse
from django.views.generic import DetailView
from django.views.generic import (ListView, CreateView, UpdateView, DeleteView,
                                  View)

from .forms import ProgramForm, MatterForm, AcademicOfferForm, SyllabusForm
from .models import Program, Matter, AcademicOffer, Syllabus


class ProgramListView(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    model = Program
    permission_required = ('syllabus.view_program',)
    template_name = 'program_home.html'


class ProgramCreateView(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    model = Program
    form_class = ProgramForm
    success_url = reverse_lazy('syllabus:program_home')
    template_name = 'program_add_edit.html'
    permission_required = ('syllabus.add_program',)

    def form_valid(self, form):
        program = form.save(commit=False)
        program.created_by = self.request.user
        program.updated_by = self.request.user
        program.save()
        messages.success(self.request, 'Programa creado con exito')
        return super(ProgramCreateView, self).form_valid(form)


class ProgramEditView(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    model = Program
    form_class = ProgramForm
    success_url = reverse_lazy('syllabus:program_home')
    template_name = 'program_add_edit.html'
    permission_required = ('syllabus.change_program',)

    def form_valid(self, form):
        program = form.save(commit=False)
        program.updated_by = self.request.user
        program.save()
        messages.success(self.request, 'Programa actualizado con exito')
        return super(ProgramEditView, self).form_valid(form)


class MatterListView(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    model = Matter
    permission_required = ('syllabus.view_matter',)
    template_name = 'matter_home.html'


class MatterCreateView(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    model = Matter,
    form_class = MatterForm
    success_url = reverse_lazy('syllabus:matter_home')
    permission_required = ('syllabus.add_matter',)
    template_name = 'matter_add_edit.html'

    def form_valid(self, form):
        matter = form.save(commit=False)
        matter.created_by = self.request.user
        matter.updated_by = self.request.user
        matter.save()
        messages.success(self.request, 'Materia creada con éxito')
        return super(MatterCreateView, self).form_valid(form)


class MatterEditView(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    model = Matter
    form_class = MatterForm
    success_url = reverse_lazy('syllabus:matter_home')
    permission_required = ('syllabus.change_matter',)
    template_name = 'matter_add_edit.html'

    def form_valid(self, form):
        matter = form.save(commit=False)
        matter.updated_by = self.request.user
        matter.save()
        messages.success(self.request, 'Materia actualizada con éxito')
        return super(MatterEditView, self).form_valid(form)


class AcademicOfferListView(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    model = AcademicOffer
    permission_required = ('syllabus.view_academicoffer',)
    template_name = 'academic_offer_home.html'


class AcademicOfferCreateView(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    model = AcademicOffer
    form_class = AcademicOfferForm
    success_url = reverse_lazy('syllabus:academic_offer_home')
    permission_required = ('syllabus.add_academicoffer',)
    template_name = 'academic_offer_add_edit.html'

    def form_valid(self, form):
        acadmic_offer = form.save(commit=False)
        acadmic_offer.created_by = self.request.user
        acadmic_offer.updated_by = self.request.user
        acadmic_offer.save()
        messages.success(self.request, 'Periodo academico creado con éxito')
        return super(AcademicOfferCreateView, self).form_valid(form)


class AcademicOfferEditView(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    model = AcademicOffer
    form_class = AcademicOfferForm
    success_url = reverse_lazy('syllabus:academic_offer_home')
    permission_required = ('syllabus.change_academicoffer',)
    template_name = 'academic_offer_add_edit.html'

    def form_valid(self, form):
        academic_offer = form.save(commit=False)
        academic_offer.updated_by = self.request.user
        academic_offer.save()
        messages.success(self.request, 'Periodo academico actualizado con éxito')
        return super(AcademicOfferEditView, self).form_valid(form)


# class SyllabusListView(LoginRequiredMixin, PermissionsRequiredMixin, ListView):
#     model = Syllabus
#     required_permissions = ('syllabus.syllabus_view',)
#     template_name = 'syllabus_home.html'


# class SyllabusCreateView(LoginRequiredMixin, PermissionsRequiredMixin, CreateView):
#     model = Syllabus
#     form_class = SyllabusForm
#     success_url = reverse_lazy('syllabus:syllabus_home')
#     required_permissions = ('syllabus.syllabus_add',)
#     template_name = 'syllabus_add_edit.html'
#     formset = modelformset_factory(model, exclude=('created_by', 'updated_by', 'syllabus',))
#
#     def get_context_data(self, **kwargs):
#         context = super(SyllabusCreateView, self).get_context_data(**kwargs)
#         if self.request.POST:
#             context['formset'] = self.formset(self.request.POST)
#         else:
#             context['formset'] = self.formset()
#         return context
#
#     def form_valid(self, form):
#         syllabus = form.save(commit=False)
#         syllabus.created_by = self.request.user
#         syllabus.updated_by = self.request.user
#         syllabus.save()
#         messages.success(self.request, 'El pensum ha sido creado con éxito')
#         return super(SyllabusCreateView, self).form_valid(form)


class SyllabusView(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    model = Syllabus
    form_class = SyllabusForm
    template_name = 'syllabus_home.html'
    permission_required = ('syllabus.view_syllabus', )

    def form_valid(self, form):
        program = AcademicOffer.objects.get(pk=self.kwargs['pgr'])

        if self.request.is_ajax():
            pensum = form.save(commit=False)
            pensum.created_by = self.request.user
            pensum.updated_by = self.request.user
            pensum.syllabus = program
            pensum.save()
            return JsonResponse({'status': 200})
        else:
            return JsonResponse({'status': 403})
        # super(SyllabusView, self).form_valid(form)

    def form_invalid(self, form):
        return JsonResponse({'status': 500})

    def get_context_data(self, **kwargs):
        program = AcademicOffer.objects.get(pk=self.kwargs['pgr'])
        pensum = Syllabus.objects.filter(syllabus=program.pk)
        context = super(SyllabusView, self).get_context_data(**kwargs)
        context['pensum'] = pensum
        context['program'] = program.id
        return context


class SyllabusEditView(LoginRequiredMixin, PermissionRequiredMixin,
                       UpdateView):
    model = Syllabus
    form_class = SyllabusForm
    template_name = 'syllabus_home.html'
    permission_required = ('syllabus.change_syllabus',)

    def form_valid(self, form):
        program = AcademicOffer.objects.get(pk=self.kwargs['pgr'])

        if self.request.is_ajax():
            pensum = form.save(commit=False)
            pensum.created_by = self.request.user
            pensum.updated_by = self.request.user
            pensum.syllabus = program
            pensum.save()
            return JsonResponse({'status': 200})
        else:
            return JsonResponse({'status': 403})
            # super(SyllabusView, self).form_valid(form)

    def form_invalid(self, form):
        return JsonResponse({'status': 500})


class SyllabusAjaxView(LoginRequiredMixin, PermissionRequiredMixin,
                       DetailView):
    model = Syllabus
    permission_required = ('syllabus.change_syllabus',)
    template_name = ''

    def get(self, request, *args, **kwargs):
        if self.request.is_ajax:
            syllabus = Syllabus.objects.get(id=self.kwargs['pgr'])
            data = {
                'id': syllabus.id,
                'syllabus': syllabus.syllabus_id,
                'matter': syllabus.matter_id,
                'hours': syllabus.hours,
                'hours_week': syllabus.hours_week,
                'date_init': syllabus.date_init,
                'date_end': syllabus.date_end,
                'day_week': syllabus.day_week,
                'schedule': syllabus.schedule,
                'teacher': syllabus.teacher_id,
                'limit_report': syllabus.limit_report,
            }
            return JsonResponse(data=data, status=200)


class SyllabusDeleteView(LoginRequiredMixin, PermissionRequiredMixin, DeleteView):
    model = Syllabus
    permission_required = ('syllabus.delete_syllabus',)
    # success_url = reverse_lazy('syllabus:syllabus_add')
    template_name = 'syllabus_delete.html'

    def get_success_url(self):
        return reverse_lazy('syllabus:syllabus_add', kwargs={'pgr': self.kwargs['pgr']})
        # super(SyllabusDeleteView, self).get_success_url(**kwargs)
