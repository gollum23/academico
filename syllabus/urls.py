from django.conf.urls import url

from .views import (ProgramListView, ProgramCreateView, ProgramEditView,
                    MatterListView, MatterCreateView, MatterEditView,
                    AcademicOfferListView, AcademicOfferCreateView,
                    AcademicOfferEditView, SyllabusView, SyllabusAjaxView,
                    SyllabusDeleteView, SyllabusEditView)

urlpatterns = [
    url(r'^curso/$', ProgramListView.as_view(), name='program_home'),
    url(r'^curso/agregar/$', ProgramCreateView.as_view(), name='program_add'),
    url(r'^curso/editar/(?P<pk>\d+)/$', ProgramEditView.as_view(),
        name='program_edit'),
    url(r'^materia/$', MatterListView.as_view(), name='matter_home'),
    url(r'^materia/agregar/$', MatterCreateView.as_view(), name='matter_add'),
    url(r'^materia/editar/(?P<pk>\d+)/$', MatterEditView.as_view(),
        name='matter_edit'),
    url(r'^periodo/$', AcademicOfferListView.as_view(),
        name='academic_offer_home'),
    url(r'^periodo/agregar/$', AcademicOfferCreateView.as_view(),
        name='academic_offer_add'),
    url(r'^periodo/editar/(?P<pk>\d+)/$', AcademicOfferEditView.as_view(),
        name='academic_offer_edit'),
    url(r'^programa/(?P<pgr>\d+)/$', SyllabusView.as_view(),
        name='syllabus_add'),
url(r'^programa/(?P<pgr>\d+)/edit/(?P<pk>\d+)/$', SyllabusEditView.as_view(),
        name='syllabus_edit'),
    url(r'^programa/(?P<pgr>\d+)/borrar-materia/(?P<pk>\d+)/$',
        SyllabusDeleteView.as_view(), name='syllabus_delete'),
    url(r'^programa/get/(?P<pgr>\d+)/$', SyllabusAjaxView.as_view(),
        name='syllabus_get_ajax')
]
