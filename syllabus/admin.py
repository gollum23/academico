from django.contrib import admin
from syllabus.models import Matter, Program, Syllabus


@admin.register(Program)
class PeriodModelAdmin(admin.ModelAdmin):
    pass


@admin.register(Matter)
class MatterModelAdmin(admin.ModelAdmin):
    pass


@admin.register(Syllabus)
class SyllabusModelAdmin(admin.ModelAdmin):
    pass
