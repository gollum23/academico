from datetime import date
from django.db import models

from helpers import dictionaries
from home.models import TimeStampUser
from teachers.models import Teacher

from multiselectfield import MultiSelectField


class Program(TimeStampUser):
    """
    Model with programs inherit from TimeStampUser Abstract Class
    """
    name_program = models.CharField(
        verbose_name='Nombre del programa',
        max_length=80
    )

    class Meta:
        verbose_name = 'Programa'
        verbose_name_plural = 'Programas'
        default_permissions = ('add', 'change', 'delete', 'view',)

    def __str__(self):
        return '{}'.format(self.name_program)


class Matter(TimeStampUser):
    """
    Model with matters and code inherit from TimeStampUser Abstract Class
    """
    name_matter = models.CharField(
        verbose_name='Nombre de la materia',
        max_length=50
    )
    cod_matter = models.CharField(
        verbose_name='Código de la materia',
        max_length=10
    )

    class Meta:
        verbose_name = 'Materia'
        verbose_name_plural = 'Materias'
        default_permissions = ('add', 'change', 'delete', 'view',)

    def __str__(self):
        return '{}'.format(self.name_matter)


class AcademicOffer(TimeStampUser):
    """
    Model with academic offer inherit from TimeStampUser Abstract Class
    """
    program = models.ForeignKey(
        Program,
        verbose_name='Programa'
    )
    year = models.SmallIntegerField(
        verbose_name='Año'
    )
    day_trip = models.CharField(
        verbose_name='Jornada',
        max_length=1,
        choices=dictionaries.DAY_TRIP
    )
    is_current = models.BooleanField(
        verbose_name='Programa activo',
        default=True
    )
    total_hours = models.IntegerField(
        verbose_name='Horas totales del programa',
        default=0
    )
    week_hours = models.IntegerField(
        verbose_name='Horas a la semana',
        default=0
    )

    class Meta:
        verbose_name = 'Oferta acádemica'
        verbose_name_plural = 'Oferta acádemica'
        default_permissions = ('add', 'change', 'delete', 'view',)

    def __str__(self):
        return '{} - {} - {}'.format(self.program.name_program, self.year,
                                     self.get_day_trip_display())

    def get_description(self):
        return '{} - {} - {}'.format(self.program.name_program, self.year,
                                     self.get_day_trip_display())


class Syllabus(TimeStampUser):
    """
    Model with syllabus inherit from TimeStampUser Abstract Class
    """
    syllabus = models.ForeignKey(
        AcademicOffer,
        verbose_name='Programa'
    )
    matter = models.ForeignKey(
        Matter,
        verbose_name='Materia'
    )
    hours = models.SmallIntegerField(
        verbose_name='# de horas resolución'
    )
    hours_week = models.SmallIntegerField(
        verbose_name='# de horas semana'
    )
    date_init = models.DateField(
        verbose_name='Inicia'
    )
    date_end = models.DateField(
        verbose_name='Termina'
    )
    day_week = MultiSelectField(
        verbose_name='Día',
        choices=dictionaries.DAYS_OF_WEEK
    )
    schedule = models.CharField(
        verbose_name='Hora',
        max_length=5
    )
    teacher = models.ForeignKey(
        Teacher,
        verbose_name='Profesor'
    )
    limit_report = models.DateField(
        verbose_name='Fecha limite reporte de información'
    )

    class Meta:
        verbose_name = 'Pensum'
        verbose_name_plural = 'Pensum'
        default_permissions = ('add', 'change', 'delete', 'view',)
        ordering = ('id', )

    def __str__(self):
        return '{} - {} - {}'.format(self.syllabus.program.name_program,
                                     self.matter.name_matter,
                                     self.teacher.get_full_name())

    @property
    def is_past_due(self):
        if date.today() > self.limit_report:
            return True
        return False
