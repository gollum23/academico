from django import forms
from django.contrib.auth.forms import AuthenticationForm


class CustomAuthenticationForm(AuthenticationForm):
    """
    Custom form authentication
    """
    class Meta:
        widgets = {
            'username': forms.TextInput(attrs={'placeholder': 'Usuario', 'class': 'FormLogin-input--user'}),
            'password': forms.PasswordInput(attrs={'placeholder': 'Contraseña', 'class': 'FormLogin-input--pass'})
        }
