from django.contrib.auth import login, logout
from django.core.urlresolvers import reverse_lazy
from django.views.generic import FormView, ListView, RedirectView, TemplateView

from .forms import CustomAuthenticationForm
from .mixins import LoginRequiredMixin


class HomeView(FormView, TemplateView):
    success_url = reverse_lazy('home:home')
    form_class = CustomAuthenticationForm

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            # TODO: information dashboard
            self.template_name = 'home--logged.html'
        else:
            self.template_name = 'home--login.html'
        return super(HomeView, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        if form.get_user():
            login(self.request, form.get_user())
        return super(HomeView, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(HomeView, self).get_context_data(**kwargs)
        if not self.request.user.is_authenticated():
            context['form_login'] = self.form_class
        return context


class LogoutView(LoginRequiredMixin, RedirectView):
    permanent = True
    url = reverse_lazy('home:home')

    def get(self, request, *args, **kwargs):
        logout(request)
        return super(LogoutView, self).get(request, *args, **kwargs)

