from django.contrib.auth.models import User
from django.db import models


class TimeStampUser(models.Model):
    created_at = models.DateTimeField(
        auto_now_add=True
    )
    updated_at = models.DateTimeField(
        auto_now=True
    )
    created_by = models.ForeignKey(
        User,
        related_name='%(app_label)s_%(class)s_ownership'
    )
    updated_by = models.ForeignKey(
        User,
        related_name='%(app_label)s_%(class)s_updated'
    )

    class Meta:
        abstract = True


class Department(models.Model):
    cod = models.PositiveSmallIntegerField(
        verbose_name='Código',
        unique=True,
        primary_key=True
    )
    name = models.CharField(
        verbose_name='Nombre',
        max_length=30
    )

    def __str__(self):
        return '{} - {}'.format(self.cod, self.name)

    class Meta:
        verbose_name = 'Departamento'
        verbose_name_plural = 'Departamentos'
        ordering = ['cod']


class Town(models.Model):
    cod_town = models.PositiveIntegerField(
        verbose_name='Código',
        unique=True,
        primary_key=True
    )
    department = models.ForeignKey(
        Department,
        verbose_name='Departamento'
    )
    name_town = models.CharField(
        verbose_name='Nombre',
        max_length=30
    )

    def __str__(self):
        return '{} - {}'.format(self.cod_town, self.name_town)

    class Meta:
        verbose_name = 'Municipio'
        verbose_name_plural = 'Municipios'
        ordering = ['name_town']
