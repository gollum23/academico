from django.conf.urls import url

from .views import HomeView, LogoutView

urlpatterns = [
    url(r'^$', HomeView.as_view(), name='home'),
    url(r'^logout/$', LogoutView.as_view(), name='logout'),
]

