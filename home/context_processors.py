from django.core.urlresolvers import reverse


def menu(request):
    items_menu = {
        # 'menu': [
        #     {'name': 'Dashboard', 'url': reverse('home:home'), 'icon': 'dashboard'},
        #     {'name': 'Estudiantes', 'url': reverse('student:list'), 'icon': 'user'},
        #     {'name': 'Profesores', 'url': '', 'icon': 'user-md', 'submenu': [
        #         {'name': 'Listado', 'url': reverse('teacher:list'), 'icon': 'list'},
        #         {'name': 'Agregar', 'url': reverse('teacher:add'), 'icon': 'plus-circle'},
        #         {'name': 'Asignación', 'url': reverse('teacher:add'), 'icon': 'exchange'},
        #     ]},
        # ]
        'menu_assistant': [
            {'name': 'Dashboard', 'url': reverse('home:home'), 'icon': 'dashboard'},
            {'name': 'Estudiantes', 'url': reverse('student:list'), 'icon': 'graduation-cap'},
            {'name': 'Profesores', 'url': reverse('teacher:list'), 'icon': 'user'},
            {'name': 'Cursos', 'url': reverse('syllabus:program_home'), 'icon': 'file-text-o'},
            {'name': 'Materias', 'url': reverse('syllabus:matter_home'), 'icon': 'book'},
            {'name': 'Programas', 'url': reverse('syllabus:academic_offer_home'), 'icon': 'university'},
            {'name': 'Matriculas', 'url': reverse('enrollment:home'), 'icon': 'check-circle'},
        ],
        'menu_teachers': [
            {'name': 'Dashboard', 'url': reverse('home:home'), 'icon': 'dashboard'},
            {'name': 'Calificaciones', 'url': reverse('ratings:list'), 'icon': ''},
            {'name': 'Asistencia', 'url': reverse('attendance:list'), 'icon': 'map'},
        ],
    }

    for item in items_menu['menu_assistant']:
        if request.path == item['url']:
            item['active'] = True
        try:
            if item['submenu']:
                for subitem in item['submenu']:
                    if request.path == subitem['url']:
                        subitem['active'] = True
        except KeyError:
            pass

    for item in items_menu['menu_teachers']:
        if request.path == item['url']:
            item['active'] = True
        try:
            if item['submenu']:
                for subitem in item['submenu']:
                    if request.path == subitem['url']:
                        subitem['active'] = True
        except KeyError:
            pass

    return items_menu
