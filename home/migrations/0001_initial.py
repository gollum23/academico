# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Department',
            fields=[
                ('cod', models.PositiveSmallIntegerField(unique=True, verbose_name='Código', serialize=False, primary_key=True)),
                ('name', models.CharField(verbose_name='Nombre', max_length=30)),
            ],
            options={
                'verbose_name_plural': 'Departamentos',
                'ordering': ['cod'],
                'verbose_name': 'Departamento',
            },
        ),
        migrations.CreateModel(
            name='Town',
            fields=[
                ('cod_town', models.PositiveIntegerField(unique=True, verbose_name='Código', serialize=False, primary_key=True)),
                ('name_town', models.CharField(verbose_name='Nombre', max_length='30')),
                ('department', models.ForeignKey(verbose_name='Departamento', to='home.Department')),
            ],
            options={
                'verbose_name_plural': 'Municipios',
                'ordering': ['department', 'name_town'],
                'verbose_name': 'Municipio',
            },
        ),
    ]
