from django.db import models

from home.models import TimeStampUser
from student.models import Student
from syllabus.models import Syllabus


class RateNotes(TimeStampUser):
    student = models.ForeignKey(
        Student,
        verbose_name='Estudiante'
    )
    matter = models.ForeignKey(
        Syllabus,
        null=True
    )
    qualification = models.DecimalField(
        verbose_name='Calificación',
        decimal_places=1,
        max_digits=2
    )

    class Meta:
        verbose_name = 'Calificación'
        verbose_name_plural = 'Calificaciones'
        default_permissions = ('add', 'change', 'delete', 'view',)

    def __str__(self):
        return '{} - {} - {}'.format(self.student.get_full_name_student(), self.matter, self.qualification)
