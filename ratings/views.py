from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.core.urlresolvers import reverse_lazy
from django.forms import modelformset_factory, inlineformset_factory
from django.http import HttpResponseRedirect
from django.views.generic import ListView, CreateView, TemplateView, UpdateView, FormView

from ratings.forms import RateNotesForm, RateNotesEditForm
from syllabus.models import Syllabus

from .models import RateNotes


class ListMattersView(ListView):
    model = Syllabus
    template_name = 'matter_list.html'

    def get_queryset(self):
        queryset = self.model.objects.filter(teacher__user_id=self.request.user.id)
        # queryset = self.model.objects.filter(teacher_id=self.request.user.id)
        return queryset


class RateMatterView(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    model = RateNotes
    form_class = RateNotesForm
    permission_required = ('ratings.add_ratenotes', )
    template_name = 'rate_matter.html'
    success_url = reverse_lazy('ratings:list')

    def get_context_data(self, **kwargs):
        # queryset = Syllabus.objects.filter(teacher_id=self.request.user.id)
        queryset = Syllabus.objects.filter(teacher__user_id=self.request.user.id)
        matter = queryset.get(pk=self.kwargs['pk'])
        notes = self.model.objects.filter(matter=matter).__len__()
        queryset = queryset.get(pk=self.kwargs['pk']).syllabus.enrollment_set.all()
        # rate_notes_formset = modelformset_factory(self.model, self.form_class, extra=queryset.count())
        rate_notes_formset = inlineformset_factory(Syllabus, self.model, self.form_class, extra=queryset.count())

        context = super(RateMatterView, self).get_context_data(**kwargs)
        context['formset'] = rate_notes_formset(instance=matter)
        context['students'] = queryset
        context['notes'] = notes
        return context

    def post(self, request, *args, **kwargs):
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        queryset = Syllabus.objects.filter(teacher__user_id=self.request.user.id)
        queryset = queryset.get(pk=self.kwargs['pk']).syllabus.enrollment_set.all()
        rate_notes_formset = inlineformset_factory(Syllabus, self.model, self.form_class, extra=queryset.count())
        formset = rate_notes_formset(self.request.POST)
        if formset.is_valid():
            return self.form_valid(form, formset)
        else:
            return self.form_invalid(form, formset)

    def form_valid(self, form, formset):
        matter = Syllabus.objects.get(pk=self.kwargs['pk'])
        for f in formset:
            f.save(commit=False)
            f.instance.created_by = self.request.user
            f.instance.updated_by = self.request.user
            f.instance.matter = matter
            f.save()
        return HttpResponseRedirect(self.success_url)

    def form_invalid(self, form, formset):
        return self.render_to_response(
            self.get_context_data(form=form,
                                  formset=formset))


class RateMatterEditView(LoginRequiredMixin, PermissionRequiredMixin, FormView):
    model = Syllabus
    form_class = RateNotesEditForm
    permission_required = ('ratings.change_ratenotes', )
    template_name = 'rate_matter_edit.html'
    success_url = reverse_lazy('ratings:list')
    formset = None

    def get_context_data(self, **kwargs):
        queryset = RateNotes.objects.filter(matter=self.kwargs['pk'])
        self.formset = modelformset_factory(RateNotes, self.form_class, extra=0)
        context = super(RateMatterEditView, self).get_context_data(**kwargs)
        context['rates'] = queryset
        context['formset'] = self.formset(queryset=queryset)
        return context

    def post(self, request, *args, **kwargs):
        form_class = self.get_form_class()
        form = self.get_form(form_class)

        rate_notes_formset = modelformset_factory(RateNotes, self.form_class, extra=0)
        formset = rate_notes_formset(self.request.POST)
        if formset.is_valid():
            return self.form_valid(form, formset)
        else:
            return self.form_invalid(form, formset)

    def form_valid(self, form, formset):
        for f in formset:
            if f.has_changed():
                f.save(commit=False)
                f.instance.updated_by = self.request.user
                f.save()
        return HttpResponseRedirect(self.success_url)

    def form_invalid(self, form, formset):
        return self.render_to_response(
            self.get_context_data(form=form,
                                  formset=formset))


