from django.conf.urls import url

from .views import ListMattersView, RateMatterView, RateMatterEditView

urlpatterns = [
    url(r'^$', ListMattersView.as_view(), name='list'),
    url(r'^calificacion/(?P<pk>\d+)/$', RateMatterView.as_view(), name='rate'),
    url(r'^calificacion/editar/(?P<pk>\d+)/$', RateMatterEditView.as_view(), name='rate-edit')
]

