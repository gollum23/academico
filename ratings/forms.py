from django import forms
from django.forms import inlineformset_factory

from syllabus.models import Syllabus
from .models import RateNotes


class RateNotesForm(forms.ModelForm):

    class Meta:
        model = RateNotes
        fields = ('student', 'qualification', )


class RateNotesEditForm(forms.ModelForm):

    class Meta:
        model = RateNotes
        fields = ('qualification', )


RatesNotesFormset = inlineformset_factory(Syllabus, RateNotes, form=RateNotesForm, can_delete=False)
