# Aplicativo academico
## Fundación Escuela Taller de Bogotá

### Modulos

- home => Controla el inicio y terminación de la sesión
- student => Controla la hoja de vida del estudiante (datos básicos), notas y asistencias
- syllabus => Programas, materias y pensum
- enrollment => Inscripción de alumnos en los program
- teacher => Datos de profesores y asignación de materias.