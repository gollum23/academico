"""academico URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin

from home import urls as home_urls
from student import urls as student_urls
from teachers import urls as teachers_urls
from syllabus import urls as syllabus_urls
from enrollment import urls as enrollment_urls
from ratings import urls as ratings_urls
from attendance import urls as attendance_urls

urlpatterns = [
    url(r'^', include(home_urls, namespace='home')),
    url(r'^estudiantes/', include(student_urls, namespace='student')),
    url(r'^profesores/', include(teachers_urls, namespace='teacher')),
    url(r'^pensum/', include(syllabus_urls, namespace='syllabus')),
    url(r'^matriculas/', include(enrollment_urls, namespace='enrollment')),
    url(r'^calificaciones/', include(ratings_urls, namespace='ratings')),
    url(r'^asistencia/', include(attendance_urls, namespace='attendance')),
    url(r'^admin/', include(admin.site.urls)),

]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
